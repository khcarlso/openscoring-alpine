FROM anapsix/alpine-java:8_jdk

MAINTAINER Kevin Carlson <kevin@carlsononcommerce.com>
# Much thanks to Anastas Dancha <anapsix@random.io> for the great alpine java images

ENV PATH=${PATH}:/opt/apache-maven-3.3.9/bin

RUN apk update && \
		apk add curl && \
		apk add git && \
		curl -o /opt/apache-maven-3.3.9-bin.tar.gz http://mirror.reverse.net/pub/apache/maven/maven-3/3.3.9/binaries/apache-maven-3.3.9-bin.tar.gz && \
		cd /opt && tar xzvf apache-maven-3.3.9-bin.tar.gz && \
		cd /opt && git clone https://github.com/openscoring/openscoring.git  && \
		cd /opt/openscoring && mvn clean install && \
		apk del curl && \
		apk del git && \
		mkdir /opt/openscoring/openscoring-client/pmml

ENTRYPOINT ["java", "-jar", "/opt/openscoring/openscoring-server/target/server-executable-1.2-SNAPSHOT.jar"]
# Change the last arg as necessary
# See openscoring.io & https://github.com/openscoring/openscoring for details
